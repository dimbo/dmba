package stub

import (
	"fmt"
	"net/http"
	"encoding/json"
	"bytes"

	"bitbucket.org/dimbo/dm_ba_rest/dto"
)

type Config struct {
	EndPoint string
}

type RestStub struct {
	cfg Config
}

func NewRestStub(cfg Config) *RestStub {
	return &RestStub{
		cfg: cfg,
	}
}

func (s *RestStub) SelectPeople() ([]*dto.Person, error) {
	response, err := http.Get(fmt.Sprintf("%s/person", s.cfg.EndPoint))
	if err != nil {
		return nil, err
	}

	people := make([]*dto.Person, 0)

	if response.StatusCode == http.StatusNotFound {
		return people, nil
	}

	if response.StatusCode >= 300 {
		return nil, fmt.Errorf(response.Status)
	}

	err = json.NewDecoder(response.Body).Decode(people)
	if err != nil {
		return nil, err
	}

	return people, nil
}

func (s *RestStub) SelectPerson(id int64) (*dto.Person, error) {
	response, err := http.Get(fmt.Sprintf("%s/person/%d", s.cfg.EndPoint, id))
	if err != nil {
		return nil, err
	}

	if response.StatusCode == http.StatusNotFound {
		return nil, nil
	}

	if response.StatusCode >= 300 {
		return nil, fmt.Errorf(response.Status)
	}

	person := &dto.Person{}

	err = json.NewDecoder(response.Body).Decode(person)
	if err != nil {
		return nil, err
	}

	return person, nil
}

func (s *RestStub) SelectPersonByEmail(email string) (*dto.Person, error) {
	url := fmt.Sprintf("%s/person/byemail/%s", s.cfg.EndPoint, email)
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	if response.StatusCode == http.StatusNotFound {
		return nil, nil
	}

	if response.StatusCode >= 300 {
		return nil, fmt.Errorf(response.Status)
	}

	person := &dto.Person{}

	err = json.NewDecoder(response.Body).Decode(person)
	if err != nil {
		return nil, err
	}

	return person, nil
}

func (s *RestStub) SelectPersonByGoogleId(googleId string) (*dto.Person, error) {
	url := fmt.Sprintf("%s/person/bygoogleid/%s", s.cfg.EndPoint, googleId)
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	if response.StatusCode == http.StatusNotFound {
		return nil, nil
	}

	if response.StatusCode >= 300 {
		return nil, fmt.Errorf(response.Status)
	}

	person := &dto.Person{}

	err = json.NewDecoder(response.Body).Decode(person)
	if err != nil {
		return nil, err
	}

	return person, nil
}

func (s *RestStub) InsertPerson(person *dto.Person) (*dto.Person, error) {
	jsonBytes, err := json.Marshal(person)
	if err != nil {
		return nil, err
	}

	response, err := http.Post(fmt.Sprintf("%s/person", s.cfg.EndPoint), "application/json", bytes.NewBuffer(jsonBytes))
	if err != nil {
		return nil, err
	}

	if response.StatusCode >= 300 {
		return nil, fmt.Errorf(response.Status)
	}

	insertedPerson := &dto.Person{}
	err = json.NewDecoder(response.Body).Decode(insertedPerson)
	if err != nil {
		return nil, err
	}

	return insertedPerson, nil
}

func (s *RestStub) DeletePerson(id int64) (bool, error) {
	req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("%s/person/%d", s.cfg.EndPoint, id), nil)
	if err != nil {
		return false, err
	}

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return false, err
	}

	if response.StatusCode >= 300 {
		return false, fmt.Errorf(response.Status)
	}

	return true, nil
}

func (s *RestStub) UpdatePerson(person *dto.Person) (*dto.Person, error) {
	jsonBytes, err := json.Marshal(person)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPut, fmt.Sprintf("%s/person/%d", s.cfg.EndPoint, person.Id), bytes.NewBuffer(jsonBytes))
	if err != nil {
		return nil, err
	}

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if response.StatusCode >= 300 {
		return nil, fmt.Errorf(response.Status)
	}

	updatedPerson := &dto.Person{}
	err = json.NewDecoder(response.Body).Decode(updatedPerson)
	if err != nil {
		return nil, err
	}

	return updatedPerson, nil
}
