package ui

import (
	"fmt"
	"html/template"
	"log"
	"net"
	"net/http"
	"path/filepath"
	"strings"
	"time"

	"github.com/dghubble/gologin"
	"github.com/dghubble/gologin/google"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"

	"golang.org/x/oauth2"
	googleOAuth2 "golang.org/x/oauth2/google"

	"bitbucket.org/dimbo/dm_ba/crypto"
	"bitbucket.org/dimbo/dm_ba/stub"
	"bitbucket.org/dimbo/dm_ba_rest/dto"
	"bitbucket.org/dimbo/dm_ba_rest/model"
)

const (
	sessionName            = "dm-ba-application"
	sessionUserGoogleIdKey = "userGoogleId"
	sessionUserIdKey       = "userId"
	sessionErrorFlashes    = "errors"
	sessionNotificationFlashes = "notifications"
)

type handlerWithSessionFunc func(http.ResponseWriter, *http.Request, *sessions.Session)

type Config struct {
	ClientID string
	ClientSecret string

	SessionStorePath string
	SessionSecret []byte
	CallbackBaseURL string
	TemplatesPath string
}

type server struct {
	sessions sessions.Store
	repo model.Repository
	tmpl *template.Template
}

func newServer(repo model.Repository, cfg *Config) *server {
	return &server {
		sessions: sessions.NewFilesystemStore(cfg.SessionStorePath, cfg.SessionSecret),
		repo: repo,
		tmpl: template.Must(template.ParseGlob(filepath.Join(cfg.TemplatesPath, "*.html"))),
	}
}

func Start(cfg Config, repo *stub.RestStub, listener net.Listener) {

	server := newServer(repo, &cfg)

	router := mux.NewRouter()

	router.HandleFunc("/", server.getIndexHandler()).Methods("GET")
	router.HandleFunc("/register", server.getRegisterUserPageHandler()).Methods("GET")
	router.HandleFunc("/register/create", server.getRegisterUserHandler()).Methods("POST")
	router.HandleFunc("/profile", server.requireLogin(server.getProfileHandler())).Methods("GET")
	router.HandleFunc("/profile/edit", server.requireLogin(server.getProfileEditHandler())).Methods("GET")
	router.HandleFunc("/profile/edit/update", server.requireLogin(server.getProfileUpdateHandler())).Methods("POST")
	router.HandleFunc("/logout", server.getLogoutHandler()).Methods("GET", "POST")
	router.HandleFunc("/login", server.getLoginHandler()).Methods("POST")

	oauth2Config := &oauth2.Config {
		ClientID: cfg.ClientID,
		ClientSecret: cfg.ClientSecret,
		RedirectURL: fmt.Sprintf("%s/google/callback", cfg.CallbackBaseURL),
		Endpoint: googleOAuth2.Endpoint,
		Scopes: []string{"profile", "email"},
	}

	stateConfig := gologin.DebugOnlyCookieConfig
	router.Handle("/google/login", google.StateHandler(stateConfig, google.LoginHandler(oauth2Config, nil)))
	router.Handle("/google/callback", google.StateHandler(stateConfig, google.CallbackHandler(oauth2Config, server.getIssueSession(), nil)))

	httpServer := &http.Server {
		ReadTimeout: 60 * time.Second,
		WriteTimeout: 60 * time.Second,
		MaxHeaderBytes: 1 << 16,
		Handler: router,
	}

	go httpServer.Serve(listener)
}

func (srv *server) getHandlerWithSession(sessionFunc handlerWithSessionFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		session, err := srv.sessions.Get(req, sessionName)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		defer session.Save(req, w)

		sessionFunc(w, req, session)
	}
}

// isAuthenticated returns true if the user has a signed session cookie.
func (srv *server) isAuthenticated(session *sessions.Session) bool {
	_, found := session.Values[sessionUserIdKey]

	return found
}

func (srv *server) getLoggedInUserId(session *sessions.Session) (int64, error) {
	userIdIface, found := session.Values[sessionUserIdKey]
	if !found {
		return 0, fmt.Errorf("key %s not found in the session", sessionUserIdKey)
	}

	userId, ok := userIdIface.(int64)
	if !ok {
		return 0, fmt.Errorf("key %s has wrong value %v", sessionUserIdKey, userIdIface)
	}

	return userId, nil
}

func (srv *server) getLoggedInUser(session *sessions.Session) (*dto.Person, error) {
	userId, err := srv.getLoggedInUserId(session)
	if err != nil {
		return nil, err
	}

	user, err := srv.repo.SelectPerson(userId)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// issueSession issues a cookie session after successful Google login
func (srv *server) getIssueSession() http.Handler {
	fn := func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		googleUser, err := google.UserFromContext(ctx)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		user, err := srv.repo.SelectPersonByGoogleId(googleUser.Id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if user == nil {
			user, err = srv.repo.SelectPersonByEmail(googleUser.Email)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			if user != nil {
				// add google id to the found user
				user.GoogleId = dto.NullString{ String: googleUser.Id, Valid: true}
				user, err = srv.repo.UpdatePerson(user)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			} else {
				// create new user
				person := &dto.Person{
					Email: googleUser.Email,
					FullName: googleUser.GivenName + " " + googleUser.FamilyName,
					GoogleId: dto.NullString {String: googleUser.Id, Valid: true},
				}

				user, err = srv.repo.InsertPerson(person)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			}
		}

		session, err := srv.sessions.Get(req, sessionName)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		session.Values[sessionUserGoogleIdKey] = googleUser.Id
		session.Values[sessionUserIdKey] = user.Id

		err = session.Save(req, w)
		if err != nil {
			log.Printf("Unable to save session: %v", err)
		}

		http.Redirect(w, req, "/profile", http.StatusFound)
	}

	return http.HandlerFunc(fn)
}

// requireLogin redirects unauthenticated users to the login route
func (srv *server) requireLogin(next http.Handler) http.HandlerFunc {
	fn := func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
		w.Header().Set("Expires", time.Now().AddDate(0, 0, -1).UTC().Format(http.TimeFormat))

		session, err := srv.sessions.Get(req, sessionName)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if !srv.isAuthenticated(session) {
			http.Redirect(w, req, "/", http.StatusFound)
			return
		}
		next.ServeHTTP(w, req)
	}
	return http.HandlerFunc(fn)
}

// route handlers
//
func (srv *server) getLoginHandler() http.HandlerFunc {
	return srv.getHandlerWithSession(func(w http.ResponseWriter, req *http.Request, session *sessions.Session) {
		if srv.isAuthenticated(session) {
			http.Redirect(w, req, "/profile", http.StatusFound)
		}

		err := req.ParseForm()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		email := strings.TrimSpace(req.PostFormValue("email"))
		if email == "" {
			session.AddFlash("Email must not be empty", sessionErrorFlashes)
			http.Redirect(w, req, "/", http.StatusFound)
			return
		}

		password := req.PostFormValue("password")
		if password == "" {
			session.AddFlash("Password must not be empty", sessionErrorFlashes)
			http.Redirect(w, req, "/", http.StatusFound)
			return
		}

		user, err := srv.repo.SelectPersonByEmail(email)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if user == nil {
			session.AddFlash("Login credentials are not valid", sessionErrorFlashes)
			http.Redirect(w, req, "/", http.StatusFound)
			return
		}

		if user.Password.Valid {
			err = crypto.CompareToHash(user.Password.String, password)
			if err != nil {
				session.AddFlash("Login credentials are not valid", sessionErrorFlashes)
				http.Redirect(w, req, "/", http.StatusFound)
				return
			}
		} else {
			session.AddFlash("Login credentials are not valid", sessionErrorFlashes)
			http.Redirect(w, req, "/", http.StatusFound)
			return
		}

		session.Values[sessionUserIdKey] = user.Id

		http.Redirect(w, req, "/profile", http.StatusFound)
	})
}

//
func (srv *server) getIndexHandler() http.HandlerFunc {
	return srv.getHandlerWithSession(func(w http.ResponseWriter, req *http.Request, session *sessions.Session) {
		if req.URL.Path != "/" {
			http.NotFound(w, req)
			return
		}

		if srv.isAuthenticated(session) {
			http.Redirect(w, req, "/profile", http.StatusFound)
			return
		}

		srv.tmpl.ExecuteTemplate(w, "login.html", map[string]interface{} {
			"title": "Login",
			"errors": session.Flashes(sessionErrorFlashes),
			"notifications": session.Flashes(sessionNotificationFlashes),
		})
	})
}

// profileHandler shows protected user content
func (srv *server) getRegisterUserPageHandler() http.HandlerFunc {
	return srv.getHandlerWithSession(func(w http.ResponseWriter, req *http.Request, session *sessions.Session) {

		srv.tmpl.ExecuteTemplate(w, "register.html", map[string]interface{} {
			"title": "Register user",
			"errors": session.Flashes(sessionErrorFlashes),
			"notifications": session.Flashes(sessionNotificationFlashes),
		})
	})
}

//
func (srv *server) getRegisterUserHandler() http.HandlerFunc {
	return srv.getHandlerWithSession(func(w http.ResponseWriter, req *http.Request, session *sessions.Session) {
		err := req.ParseForm()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		password := req.PostFormValue("password")
		passwordAgain := req.PostFormValue("password_again")
		if password != passwordAgain {
			session.AddFlash("Passwords do not match", sessionErrorFlashes)
			http.Redirect(w, req, "/register", http.StatusFound)
			return
		}

		email := strings.TrimSpace(req.PostFormValue("email"))
		if email == "" {
			session.AddFlash("Email must not be empty", sessionErrorFlashes)
			http.Redirect(w, req, "/register", http.StatusFound)
			return
		}

		fullName := strings.TrimSpace(req.PostFormValue("full_name"))
		if fullName == "" {
			session.AddFlash("Full Name must not be empty", sessionErrorFlashes)
			http.Redirect(w, req, "/register", http.StatusFound)
			return
		}

		existingUser, err := srv.repo.SelectPersonByEmail(email)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if existingUser != nil {
			session.AddFlash("The email is already used by another user", sessionErrorFlashes)
			http.Redirect(w, req, "/register", http.StatusFound)
		}

		passwordHash, err := crypto.GenerateHash(password)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		address := strings.TrimSpace(req.PostFormValue("address"))
		phoneNumber := strings.TrimSpace(req.PostFormValue("phone_number"))

		user := &dto.Person{}

		user.Email = email

		user.Password.String = passwordHash
		user.Password.Valid = true

		user.FullName = fullName

		user.Address.String = address
		user.Address.Valid = true

		user.PhoneNumber.String = phoneNumber
		user.PhoneNumber.Valid = true

		user, err = srv.repo.InsertPerson(user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		session.AddFlash("The user account has been registered successfully. You can login using the new credentials now.", sessionNotificationFlashes)
		http.Redirect(w, req, "/", http.StatusFound)
	})
}

//
func (srv *server) getProfileHandler() http.HandlerFunc {
	return srv.getHandlerWithSession(func(w http.ResponseWriter, req *http.Request, session *sessions.Session) {
		user, err := srv.getLoggedInUser(session)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		srv.tmpl.ExecuteTemplate(w, "profile-show.html", map[string]interface{} {
			"title": "Show profile",
			"errors": session.Flashes(sessionErrorFlashes),
			"notifications": session.Flashes(sessionNotificationFlashes),
			"user": user,
		})
	})
}

//
func (srv *server) getProfileEditHandler() http.HandlerFunc {
	return srv.getHandlerWithSession(func(w http.ResponseWriter, req *http.Request, session *sessions.Session) {
		user, err := srv.getLoggedInUser(session)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		srv.tmpl.ExecuteTemplate(w, "profile.html", map[string]interface{} {
			"title": "Edit profile",
			"errors": session.Flashes(sessionErrorFlashes),
			"notifications": session.Flashes(sessionNotificationFlashes),
			"user": user,
		})
	})
}

//
func (srv *server) getProfileUpdateHandler() http.HandlerFunc {
	return srv.getHandlerWithSession(func(w http.ResponseWriter, req *http.Request, session *sessions.Session) {
		err := req.ParseForm()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		user, err := srv.getLoggedInUser(session)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		email := strings.TrimSpace(req.PostFormValue("email"))
		if email == "" {
			session.AddFlash("Email must not be empty", sessionErrorFlashes)
			http.Redirect(w, req, "/profile/edit", http.StatusFound)
			return
		}

		fullName := strings.TrimSpace(req.PostFormValue("full_name"))
		if fullName == "" {
			session.AddFlash("Full Name must not be empty", sessionErrorFlashes)
			http.Redirect(w, req, "/profile/edit", http.StatusFound)
			return
		}

		existingUser, err := srv.repo.SelectPersonByEmail(email)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if existingUser != nil && existingUser.Id != user.Id {
			session.AddFlash("The email is already used by another user", sessionErrorFlashes)
			http.Redirect(w, req, "/profile/edit", http.StatusFound)
		}

		address := strings.TrimSpace(req.PostFormValue("address"))
		phoneNumber := strings.TrimSpace(req.PostFormValue("phone_number"))

		user.Email = email
		user.FullName = fullName
		user.Address.String = address
		user.Address.Valid = true
		user.PhoneNumber.String = phoneNumber
		user.PhoneNumber.Valid = true

		_, err = srv.repo.UpdatePerson(user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		session.AddFlash("The profile has been updated successfully", sessionNotificationFlashes)
		http.Redirect(w, req, "/profile", http.StatusFound)
	})
}

// logoutHandler destroys the session on POSTs and redirects to home.
func (srv *server) getLogoutHandler() http.HandlerFunc {
	return srv.getHandlerWithSession(func(w http.ResponseWriter, req *http.Request, session *sessions.Session) {
		if req.Method == "POST" {
			session, err := srv.sessions.Get(req, sessionName)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			delete(session.Values, sessionUserIdKey)
			delete(session.Values, sessionUserGoogleIdKey)
			session.AddFlash("You have logged out", sessionNotificationFlashes)
		}

		http.Redirect(w, req, "/", http.StatusFound)
	})
}
