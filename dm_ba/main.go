package main

import (
	"bitbucket.org/dimbo/dm_ba/daemon"
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
)

func processFlags() *daemon.Config {
	cfg := &daemon.Config{}

	flag.StringVar(&cfg.UI.CallbackBaseURL, "callback-base-url", os.Getenv("DM_BA_CALLBACK_BASE_URL"), "Base URL for Google Auth")
	flag.StringVar(&cfg.ListenSpec, "listen", os.Getenv("DM_BA_LISTENSPEC"), "HTTP listen spec")
	flag.StringVar(&cfg.RestStub.EndPoint, "rest-endpoint", os.Getenv("DM_BA_REST_ENDPOINT"), "REST API endpoint")
	flag.StringVar(&cfg.UI.SessionStorePath, "session-store", "", "Path to session directory")

	defaultTemplates := "./templates"
	exe, err := os.Executable()
	if err == nil {
		defaultTemplates = path.Join(filepath.Dir(exe), defaultTemplates)
	}

	flag.StringVar(&cfg.UI.TemplatesPath, "templates", defaultTemplates, "Path to templates")

	flag.Parse()

	return cfg
}

func setupHttpParams(cfg *daemon.Config) error {
	cfg.UI.ClientID = os.Getenv("DM_BA_GOOGLE_CLIENT_ID")
	cfg.UI.ClientSecret = os.Getenv("DM_BA_GOOGLE_CLIENT_SECRET")
	if len(cfg.UI.ClientID) == 0 || len(cfg.UI.ClientSecret) == 0 {
		return fmt.Errorf("google credentials are not specified")
	}

	cfg.UI.SessionSecret = []byte("session secret")
	if len(cfg.UI.SessionSecret) == 0 {
		return fmt.Errorf("session secret is not specified")
	}

	return nil
}

func main() {
	cfg := processFlags()

	if err := setupHttpParams(cfg); err != nil {
		log.Fatalf("Error in parameters: %v", err)
	}

	if err := daemon.Run(cfg); err != nil {
		log.Printf("Error in main(): %v", err)
	}
}
