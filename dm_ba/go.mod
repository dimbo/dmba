module bitbucket.org/dimbo/dm_ba

go 1.12

require (
	bitbucket.org/dimbo/dm_ba_rest v0.0.0-00010101000000-000000000000
	github.com/dghubble/gologin v2.1.0+incompatible
	github.com/gorilla/mux v1.7.2
	github.com/gorilla/sessions v1.1.3
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f
	golang.org/x/oauth2 v0.0.0-20190523182746-aaccbc9213b0
	google.golang.org/api v0.5.0 // indirect
)

replace bitbucket.org/dimbo/dm_ba_rest => ../dm_ba_rest
