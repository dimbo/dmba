package daemon

import (
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/dimbo/dm_ba/ui"
	"bitbucket.org/dimbo/dm_ba/stub"
)

type Config struct {
	ListenSpec string
	RestStub stub.Config
	UI ui.Config
}

func Run(cfg *Config) error {
	log.Printf("Starting, HTTP on: %s\n", cfg.ListenSpec)

	listener, err := net.Listen("tcp", cfg.ListenSpec)
	if err != nil {
		log.Printf("Error creating listener: %v\n", err)
		return err
	}

	restService := stub.NewRestStub(cfg.RestStub)
	ui.Start(cfg.UI, restService, listener)

	waitForSignal()

	return nil
}

func waitForSignal() {
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	s := <- ch
	log.Printf("Got signal: %v, exiting.", s)
}
