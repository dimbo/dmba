package daemon

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/dimbo/dm_ba_rest/model"
	"bitbucket.org/dimbo/dm_ba_rest/db"
	"bitbucket.org/dimbo/dm_ba_rest/rest"
)

type Config struct {
	ListenSpec string

	Db db.Config
}

func Run(cfg *Config) error {
	log.Printf("Starting REST server, HTTP on: %s\n", cfg.ListenSpec)

	dbs, err := db.InitDb(cfg.Db)
	if err != nil {
		log.Printf("Error initializing database: %v\n", err)
		return err
	}

	m := model.New(dbs)

	err = rest.Start(m, cfg.ListenSpec)
	if err != nil {
		log.Printf("Error creating listener: %v\n", err)
		return err
	}

	waitForSignal()

	return nil
}

func waitForSignal() {
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	s := <- ch
	log.Printf("Got signal: %v, exiting.", s)
}
