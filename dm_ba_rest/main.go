package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/dimbo/dm_ba_rest/daemon"
)

func processFlags() *daemon.Config {
	cfg := &daemon.Config{}

	flag.StringVar(&cfg.ListenSpec, "listen", os.Getenv("DM_BA_REST_LISTENSPEC"), "HTTP listen spec")
	flag.StringVar(&cfg.Db.ConnectString, "db-connect", os.Getenv("DM_BA_REST_DB_CONNECT_STRING"), "DB Connect String")

	flag.Parse()

	return cfg
}

func main() {
	cfg := processFlags()

	fmt.Printf("Listen on %s\n", cfg.ListenSpec)
	fmt.Printf("DB connect string: %s\n", cfg.Db.ConnectString)

	if err := daemon.Run(cfg); err != nil {
		log.Printf("Error in main(): %v", err)
	}
}
