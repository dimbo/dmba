package db

import (
	"github.com/jmoiron/sqlx"
	"github.com/iancoleman/strcase"
	_ "github.com/go-sql-driver/mysql"

	"bitbucket.org/dimbo/dm_ba_rest/dto"
)

type Config struct {
	ConnectString string
}

func InitDb(cfg Config) (*mysqlDb, error) {
	sqlx.NameMapper = strcase.ToSnake

	if dbConn, err := sqlx.Connect("mysql", cfg.ConnectString); err != nil {
		return nil, err
	} else {
		p := &mysqlDb{ dbConn: dbConn }
		if err := p.dbConn.Ping(); err != nil {
			return nil, err
		}
		if err := p.createTablesIfNotExist(); err != nil {
			return nil, err
		}
		if err := p.prepareSqlStatements(); err != nil {
			return nil, err
		}
		return p, nil
	}
}

type mysqlDb struct {
	dbConn *sqlx.DB

	sqlSelectPeople *sqlx.Stmt
	sqlInsertPerson *sqlx.NamedStmt
	sqlSelectPerson *sqlx.Stmt
	sqlSelectPersonByEmail *sqlx.Stmt
	sqlSelectPersonByGoogleId *sqlx.Stmt
	sqlDeletePerson *sqlx.Stmt
	sqlUpdatePerson *sqlx.NamedStmt
}

func (p *mysqlDb) createTablesIfNotExist() error {
	createSql := `
		create table if not exists people (
			id serial not null primary key,
			email varchar(128) not null unique,
            password text,
            full_name text not null,
            address text,
            phone_number text,
            google_id varchar(256) unique
        );
	`

	if rows, err := p.dbConn.Query(createSql); err != nil {
		return err
	} else {
		rows.Close()
	}

	return nil
}

func (p *mysqlDb) prepareSqlStatements() (err error) {
	if p.sqlSelectPeople, err = p.dbConn.Preparex("select id, email, password, full_name, address, phone_number, google_id from people order by id"); err != nil {
		return err
	}

	if p.sqlInsertPerson, err = p.dbConn.PrepareNamed("insert into people(email, password, full_name, address, phone_number, google_id) values(:email, :password, :full_name, :address, :phone_number, :google_id)"); err != nil {
		return err
	}

	if p.sqlSelectPerson, err = p.dbConn.Preparex("select id, email, password, full_name, address, phone_number, google_id from people where id = ?"); err != nil {
		return err
	}

	if p.sqlSelectPersonByEmail, err = p.dbConn.Preparex("select id, email, password, full_name, address, phone_number, google_id from people where email = ?"); err != nil {
		return err
	}

	if p.sqlSelectPersonByGoogleId, err = p.dbConn.Preparex("select id, email, password, full_name, address, phone_number, google_id from people where google_id = ?"); err != nil {
		return err
	}

	if p.sqlDeletePerson, err = p.dbConn.Preparex("delete from people where id = ?"); err != nil {
		return err
	}

	if p.sqlUpdatePerson, err = p.dbConn.PrepareNamed("update people set email = :email, password = :password, full_name = :full_name, address = :address, phone_number = :phone_number, google_id = :google_id where id = :id"); err != nil {
		return err
	}

	return nil
}

func (p *mysqlDb) SelectPeople() ([]*dto.Person, error) {
	people := make([]*dto.Person, 0)
	if err := p.sqlSelectPeople.Select(&people); err != nil {
		return nil, err
	}
	return people, nil
}

func (p *mysqlDb) SelectPerson(id int64) (*dto.Person, error) {
	people := make([]*dto.Person, 0)

	if err := p.sqlSelectPerson.Select(&people, id); err != nil {
		return nil, err
	}

	if len(people) == 0 {
		return nil, nil
	}

	return people[0], nil
}

func (p *mysqlDb) SelectPersonByEmail(email string) (*dto.Person, error) {
	people := make([]*dto.Person, 0)

	if err := p.sqlSelectPersonByEmail.Select(&people, email); err != nil {
		return nil, err
	}

	if len(people) == 0 {
		return nil, nil
	}

	return people[0], nil
}

func (p *mysqlDb) SelectPersonByGoogleId(googleId string) (*dto.Person, error) {
	people := make([]*dto.Person, 0)

	if err := p.sqlSelectPersonByGoogleId.Select(&people, googleId); err != nil {
		return nil, err
	}

	if len(people) == 0 {
		return nil, nil
	}

	return people[0], nil
}

func (p *mysqlDb) InsertPerson(person *dto.Person) (*dto.Person, error) {
	sqlResult, err := p.sqlInsertPerson.Exec(person)
	if err != nil {
		return nil, err
	}

	id, err := sqlResult.LastInsertId()
	if err != nil {
		return nil, err
	}

	reloadedPerson, err := p.SelectPerson(id)
	if err != nil {
		return nil, err
	}

	return reloadedPerson, nil
}

func (p *mysqlDb) DeletePerson(id int64) (bool, error) {
	sqlResult, err := p.sqlDeletePerson.Exec(id)
	if err != nil {
		return false, err
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		return false, err
	}

	if rowsAffected == 0 {
		return false, nil
	}

	return true, nil
}

func (p *mysqlDb) UpdatePerson(person *dto.Person) (*dto.Person, error) {
	sqlResult, err := p.sqlUpdatePerson.Exec(person)
	if err != nil {
		return nil, err
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		return nil, err
	}

	if rowsAffected == 0 {
		// rowsAffected == 0 when no actual records have been updated,
		// for example, if all person's structure member valuss
		// are equal to those stored in the database
		// So, just return the original person struct without reloading from the DB
		return person, nil
	}

	reloadedPerson, err := p.SelectPerson(person.Id)
	if err != nil {
		return nil, err
	}

	return reloadedPerson, nil
}
