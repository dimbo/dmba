package model

type Model struct {
	Repository
}

func New(db Repository) *Model {
	return &Model{
		Repository: db,
	}
}
