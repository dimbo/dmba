package model

import "bitbucket.org/dimbo/dm_ba_rest/dto"

type Repository interface {
	SelectPeople() ([]*dto.Person, error)
	SelectPerson(id int64) (*dto.Person, error)
	SelectPersonByEmail(email string) (*dto.Person, error)
	SelectPersonByGoogleId(googleId string) (*dto.Person, error)
    InsertPerson(person *dto.Person) (*dto.Person, error)
	DeletePerson(id int64) (bool, error)
	UpdatePerson(person *dto.Person) (*dto.Person, error)
}
