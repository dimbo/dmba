package rest

import (
	"github.com/gorilla/mux"

	"bitbucket.org/dimbo/dm_ba_rest/model"

	"net/http"
	"encoding/json"
	"strconv"
	"fmt"
	"bitbucket.org/dimbo/dm_ba_rest/dto"
)

func Start(m *model.Model, listenSpec string) error {
	router := mux.NewRouter()

	router.HandleFunc("/person", GetPeopleHandler(m)).Methods("GET")
	router.HandleFunc("/person/byemail/{email}", GetPersonByEmailHandler(m)).Methods("GET")
	router.HandleFunc("/person/bygoogleid/{googleid}", GetPersonByGoogleIdHandler(m)).Methods("GET")
	router.HandleFunc("/person/{id}", GetPersonHandler(m)).Methods("GET")
	router.HandleFunc("/person", CreatePersonHandler(m)).Methods("POST")
	router.HandleFunc("/person/{id}", DeletePersonHandler(m)).Methods("DELETE")
	router.HandleFunc("/person/{id}", UpdatePersonHandler(m)).Methods("PUT")

	return http.ListenAndServe(listenSpec, router)
}

func getValidId(parameters map[string]string, paramName string) (int64, error) {
	paramValue, found := parameters[paramName]
	if !found {
		return 0, fmt.Errorf("parameter %s not supplied", paramName)
	}
	n, err := strconv.ParseInt(paramValue, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("the value is not an integer: %v", paramValue)
	}

	if n <= 0 {
		return 0, fmt.Errorf("the value is less than zero: %v", n)
	}

	return n, nil
}

func GetPeopleHandler(m *model.Model) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		people, err := m.SelectPeople()
		if err != nil {
			//http.Error(w, "This is an error", http.StatusBadRequest)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

	 	json.NewEncoder(w).Encode(people)
	}
}

func GetPersonByEmailHandler(m *model.Model) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		email, found := params["email"]
		if !found {
			http.Error(w, "parameter email not supplied", http.StatusBadRequest)
			return
		}
		person, err := m.SelectPersonByEmail(email)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		if person == nil {
			http.Error(w, "Not Found", http.StatusNotFound)
			return
		}

		json.NewEncoder(w).Encode(person)
	}
}

func GetPersonByGoogleIdHandler(m *model.Model) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		googleId, found := params["googleid"]
		if !found {
			http.Error(w, "parameter googleid not supplied", http.StatusBadRequest)
			return
		}

		person, err := m.SelectPersonByGoogleId(googleId)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		if person == nil {
			http.Error(w, "Not Found", http.StatusNotFound)
			return
		}

		json.NewEncoder(w).Encode(person)
	}
}

func GetPersonHandler(m *model.Model) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := getValidId(mux.Vars(r), "id")
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		person, err := m.SelectPerson(id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		if person == nil {
			http.Error(w, "Not Found", http.StatusNotFound)
			return
		}

		json.NewEncoder(w).Encode(person)
	}
}

func CreatePersonHandler(m *model.Model) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		person := &dto.Person{}

		err := json.NewDecoder(r.Body).Decode(person)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		insertedPerson, err := m.InsertPerson(person)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		json.NewEncoder(w).Encode(insertedPerson)
	}
}

func DeletePersonHandler(m *model.Model) http.HandlerFunc {
	return func (w http.ResponseWriter, r *http.Request) {
		id, err := getValidId(mux.Vars(r), "id")
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		deleted, err := m.DeletePerson(id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		if !deleted {
			http.Error(w, "not found", http.StatusNotFound)
		}
	}
}

func UpdatePersonHandler(m *model.Model) http.HandlerFunc {
	return func (w http.ResponseWriter, r *http.Request) {
		id, err := getValidId(mux.Vars(r), "id")
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		foundPerson, err := m.SelectPerson(id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if foundPerson == nil {
			http.Error(w, "not found", http.StatusNotFound)
			return
		}

		person := &dto.Person{}
		err = json.NewDecoder(r.Body).Decode(person)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		person.Id = id  // to not override the ID

		updatedPerson, err := m.UpdatePerson(person)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		json.NewEncoder(w).Encode(updatedPerson)
	}
}