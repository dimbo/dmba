package dto

type Person struct {
	Id int64                    `json:"id"`
	Email string                `json:"email"`
	Password NullString         `json:"password"`
	FullName string             `json:"fullName"`
	Address NullString          `json:"address"`
	PhoneNumber NullString      `json:"phoneNumber"`
	GoogleId NullString         `json:"googleId"`
}
