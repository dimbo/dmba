package dto

import (
	"encoding/json"
	"database/sql"
	"reflect"
	"database/sql/driver"
)

type NullString sql.NullString

// MarshalJSON for NullString
func (ns *NullString) MarshalJSON() ([]byte, error) {
	if !ns.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(ns.String)
}

// UnmarshalJSON for NullString
func (ns *NullString) UnmarshalJSON(b []byte) error {
	var err error
	var v interface{}

	if err = json.Unmarshal(b, &v); err != nil {
		return err
	}

	switch x := v.(type) {
	case string:
		ns.Valid = true
		ns.String = x
	default:
		ns.Valid = false
		ns.String = ""
	}

	return nil
}

// Scan implements the Scanner interface for NullString
func (ns *NullString) Scan(value interface{}) error {
	var s sql.NullString
	if err := s.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ns = NullString{s.String, false}
	} else {
		*ns = NullString{s.String, true}
	}

	return nil
}

// Value implements the Valuer interface for NullString
func (ns NullString) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.String, nil
}
