module bitbucket.org/dimbo/dm_ba_rest

go 1.12

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.2
	github.com/iancoleman/strcase v0.0.0-20190422225806-e506e3ef7365
	github.com/jmoiron/sqlx v1.2.0
	google.golang.org/appengine v1.6.0 // indirect
)
